import { Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { IAppConfig } from './config/app';
import { ConfigService } from '@nestjs/config';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { APP_CONFIG, MICROSERVICE_CONFIG } from './config/constants';
async function bootstrap() {
  Logger.log('APP RATINGS STATING');
  const app = await NestFactory.create(AppModule);
  const config = app.get<IAppConfig>(ConfigService);
  app.enableCors({
    origin: 'http://localhost:3000',
  });
  app.connectMicroservice(config.get(MICROSERVICE_CONFIG));
  app.startAllMicroservices();
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
      forbidNonWhitelisted: true,
    }),
  );

  const configSwagger = new DocumentBuilder()
    .setTitle('Ratings API')
    .setDescription('This api allows tha management of the ratings')
    .setVersion('1.0')
    .addTag('')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, configSwagger);
  SwaggerModule.setup('/api/v1/docs', app, document);

  await app.listen(config.get<IAppConfig>(APP_CONFIG).httpPort);
}
bootstrap();
