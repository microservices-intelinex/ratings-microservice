import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { RatingsModule } from './ratings/ratings.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigService, ConfigModule } from '@nestjs/config';
import { MONGO_DB } from './config/constants';
import enviroments from './config/enviroments';
import app, { microserviceConfig, mongoConfig } from './config/app';
import validation from './config/validation';

const ConfigModuleProvider = ConfigModule.forRoot({
  envFilePath: enviroments[process.env.NODE_ENV] || '.env',
  isGlobal: true,
  load: [app, mongoConfig, microserviceConfig],
  validationSchema: validation,
});
const MongooseConfig = MongooseModule.forRootAsync({
  imports: [],
  inject: [ConfigService],
  useFactory: (configService: ConfigService) => {
    return configService.get(MONGO_DB);
  },
});
@Module({
  imports: [RatingsModule, ConfigModuleProvider, MongooseConfig],
  controllers: [AppController],
})
export class AppModule {}
