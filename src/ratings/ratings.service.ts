import { Injectable } from '@nestjs/common';
import { CreateRatingDto } from './dto/create-rating.dto';
import { Rating, RatingDocument } from './entities/rating.entity';

import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SearcherParamenter } from './pipes/search-paramenter.pipe';
import { Pagination } from 'src/common/interfaces/pagination.interface';
import { buildPagination } from 'src/common/pagination';
@Injectable()
export class RatingsService {
  constructor(
    @InjectModel(Rating.name) private ratingModel: Model<RatingDocument>,
  ) {}

  create(createRatingDto: CreateRatingDto) {
    return this.ratingModel.create(createRatingDto);
  }
  async findAll(pagination: Pagination, searcher: SearcherParamenter) {
    const total = await this.ratingModel.count(searcher);
    const currentPage = (pagination.page - 1) * pagination.limit;
    const data = await this.ratingModel
      .find(searcher)
      .limit(pagination.limit)
      .skip(currentPage);
    return buildPagination(
      data,
      pagination.page,
      pagination.limit,
      total,
      currentPage,
    );
  }

  removeByISNB(bookISNB: string) {
    return this.ratingModel.deleteMany({
      bookISNB,
    });
  }
}
