import { Controller, Get, Post, Body, Query, Logger } from '@nestjs/common';
import { RatingsService } from './ratings.service';
import { CreateRatingDto } from './dto/create-rating.dto';
import {
  ApiQueryISBN,
  ApiQueryLimit,
  ApiQueryPage,
  ApiQueryUserId,
} from 'src/common/decorators/api-query.pagination';
import { ApiOperation } from '@nestjs/swagger';
import { PaginationPipe } from 'src/common/pipes/pagination.pipe';
import {
  SearcherParamenter,
  SearchParametersPipe,
} from './pipes/search-paramenter.pipe';
import { EventPattern, Payload } from '@nestjs/microservices';
import { Pagination } from 'src/common/interfaces/pagination.interface';
import { BOOK_DELETED } from 'src/config/events';
import * as Bluebird from 'bluebird';

@Controller('ratings')
export class RatingsController {
  constructor(private readonly ratingsService: RatingsService) {}

  @ApiOperation({
    summary: 'Allows create a new rating associate a book and user',
  })
  @Post()
  create(@Body() createRatingDto: CreateRatingDto) {
    return this.ratingsService.create(createRatingDto);
  }

  @ApiOperation({
    summary: 'Allows find ratings by userid and book id including pagination',
  })
  @ApiQueryLimit()
  @ApiQueryPage()
  @ApiQueryISBN()
  @ApiQueryUserId()
  @Get()
  findAll(
    @Query(PaginationPipe) pagination: Pagination,
    @Query(SearchParametersPipe) searcher: SearcherParamenter,
  ) {
    return this.ratingsService.findAll(pagination, searcher);
  }

  @EventPattern(BOOK_DELETED)
  async removeRatingsByBook(@Payload('ISBN') bookISNB: string) {
    if (!bookISNB) {
      Logger.error('The ISBN is invalid');
    }
    Logger.debug('Eliminando todos los registros', bookISNB);
    await this.ratingsService.removeByISNB(bookISNB);
    Logger.debug('eliminacion finalizada', bookISNB);
  }
}
