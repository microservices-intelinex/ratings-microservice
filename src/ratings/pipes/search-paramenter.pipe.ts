import {
  ArgumentMetadata,
  ForbiddenException,
  Injectable,
  Logger,
  PipeTransform,
} from '@nestjs/common';

export interface SearcherParamenter {
  bookISNB?: string;
  userId?: string;
}

@Injectable()
export class SearchParametersPipe implements PipeTransform {
  transform(value: any, _metadata: ArgumentMetadata): SearcherParamenter {
    try {
      const searcher: SearcherParamenter = {};
      if (!value) {
        return searcher;
      }
      if (value.bookISNB) {
        searcher.bookISNB = value.bookISNB;
      }
      return searcher;
    } catch (error) {
      new Logger(SearchParametersPipe.name).error(error);
      throw new ForbiddenException(error.message);
    }
  }
}
