import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString, IsUrl } from 'class-validator';
import { Document } from 'mongoose';

export type RatingDocument = Rating & Document;

@Schema()
export class Rating {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @Prop()
  userId: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @Prop()
  bookISNB: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  @Prop()
  rating: number;

  @Prop({
    default: new Date(),
  })
  createdAt?: Date;
}

export const RatingSchema = SchemaFactory.createForClass(Rating);
